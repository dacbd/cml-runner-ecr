#!/bin/bash

docker pull ghcr.io/iterative/cml:latest

docker tag ghcr.io/iterative/cml:latest 342840881361.dkr.ecr.us-west-1.amazonaws.com/temp1:latest
docker tag ghcr.io/iterative/cml:latest 342840881361.dkr.ecr.us-west-1.amazonaws.com/temp2:latest

docker push 342840881361.dkr.ecr.us-west-1.amazonaws.com/temp1:latest
docker push 342840881361.dkr.ecr.us-west-1.amazonaws.com/temp2:latest
